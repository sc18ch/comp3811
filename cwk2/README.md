# COMP3811 Computer Graphics - Coursework 2

## Requirements
This project requires c++17 and Qt5. The other libraries used are included in the 'lib' folder

## Running the application
To run the application, navigate to the 'code' folder and enter the following commands in a terminal

```qmake```
```make```
```./cwk2```
