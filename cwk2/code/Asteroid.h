#pragma once

#include "../libs/glm/glm/glm.hpp"

// Defines the Asteroid object, which holds information on properties of an asteroid
class Asteroid {
    public:
        Asteroid(float x, float y, float z, glm::vec3 spinAxis, int spinSpeed, glm::vec3 scale) {
            this->x = x;
            this->y = y;
            this->z = z;
            this->spinAxis = spinAxis;
            this->spinSpeed = spinSpeed;
            this->scale = scale;
        }

        float x;
        float y;
        float z;
        glm::vec3 spinAxis;
        int spinSpeed;
        glm::vec3 scale;
};