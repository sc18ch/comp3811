#pragma once

#include "../libs/glm/glm/glm.hpp"
#include <vector>
#include "Camera.h"

// Derived class from Camera, for camera with movement with target location to fix look on
class CameraMoveable : public Camera {
    public:
        // Methods
        CameraMoveable(std::vector<float> position, float cameraSpeed);
        CameraMoveable() = default;
        glm::mat4 GetLookAtMatrix();
        void ProcessMouseMove(int previousX, int previousY, int x, int y);
        void MoveForward();
        void MoveBackward();
        void MoveLeft();
        void MoveRight();

        // Member variables
        glm::vec3 cameraUp;
        glm::vec3 cameraFront;
        float cameraSpeed;
        float yaw = -90;
        float pitch = 0;
};