#pragma once

#include "../libs/glm/glm/glm.hpp"

// Base Camera class
class Camera {
    public:
        virtual glm::mat4 GetLookAtMatrix() = 0;
        virtual void ProcessMouseMove(int previousX, int previousY, int x, int y){}

        glm::vec3 cameraPosition;
};