#include "../libs/glm/glm/glm.hpp"
#include "../libs/glm/glm/ext/matrix_transform.hpp"
#include "CameraTarget.h"
#include <vector>

using namespace std;

CameraTarget::CameraTarget(vector<float> position, vector<float> target) {

    // Setting up camera position, target location of where it's looking and direction vector of where it's facing
    cameraPosition = glm::vec3(position[0], position[1], position[2]);
    cameraTarget = glm::vec3(target[0], target[1], target[2]);
    cameraDirection = glm::normalize(cameraPosition - cameraTarget);
    
}

// Returns the look at matrix for the camera
glm::mat4 CameraTarget::GetLookAtMatrix() {
    return glm::lookAt(cameraPosition, cameraTarget, glm::vec3(0, 1, 0));
}