#include <GL/glu.h>
#include <QGLWidget>
#include <QKeyEvent>
#include <QPoint>
#include <QApplication>
#include <cmath>
#include "Widget.h"
#include "Materials.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cstdlib>
#include "../libs/glm/glm/glm.hpp"
#include "../libs/glm/glm/gtc/type_ptr.hpp"
#include "../libs/glm/glm/gtx/transform.hpp"
#include "../libs/glm/glm/gtx/rotate_vector.hpp"
#include "../libs/glm/glm/gtx/vector_angle.hpp"
#include "CameraTarget.h"
#include "CameraMoveable.h"
#include "Asteroid.h"

using namespace glm;

static const int circleFidelity = 50; // This determines the number of faces of the cylinder

static std::vector<vec3> unitTriangle = {
  {-0.5, -1 * ((glm::tan(glm::radians(60.0f)) * 0.5) / 2), 0},
  {0, (glm::tan(glm::radians(60.0f)) * 0.5) / 2, 0},
  {0.5, -1 * ((glm::tan(glm::radians(60.0f)) * 0.5) / 2), 0}
};

static std::vector<vec3> unitCube = {
  {-0.5, -0.5, 0.5},
  {0.5, -0.5, 0.5},
  {0.5, 0.5, 0.5},
  {-0.5, 0.5, 0.5},
  {-0.5, -0.5, -0.5},
  {0.5, -0.5, -0.5},
  {0.5, 0.5, -0.5},
  {-0.5, 0.5, -0.5}
};

// Setting default camera to use
Widget::Widget(QWidget *parent): QGLWidget(parent), camera(cameraMoveable) {
  this->setFocusPolicy(Qt::StrongFocus);
}

// Called when the OpenGL context is set up
void Widget::initializeGL() {

	// Sets the widget background colour
	glClearColor(0.1, 0.1, 0.1, 0.0);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_NORMALIZE);

  // Perspective camera
  gluPerspective(45, 1, 0.001, 200);
  cameraTarget = CameraTarget({0, 4, 5}, {0, 0, 0});
  cameraMoveable = CameraMoveable({0, 2, 5}, 1);

  // Initialising textures
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  this->textures.insert(std::make_pair("markus", Texture("markus.jpg")));
  this->textures.insert(std::make_pair("marc_de_kamps", Texture("Marc_Dekamps.jpg")));
  this->textures.insert(std::make_pair("map", Texture("Mercator-projection.jpg")));
  this->textures.insert(std::make_pair("space", Texture("space.jpg")));
  this->textures.insert(std::make_pair("asteroid", Texture("asteroid.jpg")));
  this->textures.insert(std::make_pair("spaceship", Texture("spaceship.jpg")));
  this->textures.insert(std::make_pair("alien", Texture("alien.jpg")));
  this->textures.insert(std::make_pair("solar_panel", Texture("solar_panel.jpg")));
  this->textures.insert(std::make_pair("eye", Texture("eye.jpg")));

  // Generate Asteroids
  generateAsteroids();
}

// Called every time the widget needs painting
void Widget::paintGL() {

  // Redraw light each frame - this is done due to camera movement causing light location to move
  processLighting();

  processMouseMove();

	// Clear the buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test
  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST);
	
	glLoadIdentity();
  glLoadMatrixf(value_ptr(camera.GetLookAtMatrix()));

  // Draw skybox
  drawSkybox();

  // Draw lightPosition if enabled
  if (showLightPosition) {drawLightPosition();}

  // Draw axis if enabled
  if (showAxis) {drawAxis();}

  // Start drawing scene below
  drawScene();

	// Flush buffer to screen
	glFlush();
}

// Draws a unit cube around the camera and disabling depth buffer, giving the illusion of the scene in space
void Widget::drawSkybox() {
  // Disable wireframe when drawing skybox
  bool wireframeOn = wireframeEnabled;
  wireframeEnabled = false;

  glDepthMask(GL_FALSE);

  glEnable(GL_TEXTURE_2D);
  textures.at("space").bindTexture();
  cube(true, &texture, getTransformMatrix(camera.cameraPosition, vec3(1, 1, 1), 0, vec3(1, 1, 1)));
  glDisable(GL_TEXTURE_2D);

  glDepthMask(GL_TRUE);
  wireframeEnabled = wireframeOn;
}

// Sets up all the objects in the scene
void Widget::drawScene() {
  glPushMatrix();

  // Draw Asteroid belt
  drawAsteroids();

  // Center object of solar system - uses markus texture
  drawSpaceStation();

  // First orbiting object of solar system - red and black spaceship thing
  drawSpaceship();

  // Second orbiting object(s) of solar system - planet and marc de kamps moon
  drawPlanet();

  // Final object and new orbit - alien thing
  drawAlien();
  
  glPopMatrix();
}

// Draws center object Markus International Space Station (M.I.S.S)
void Widget::drawSpaceStation() {
  glPushMatrix();

  textures.at("markus").bindTexture();

  mat4 transformation = getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 0), _time, vec3(1, 1, 1));
  glMultMatrixf(value_ptr(transformation));

  cube(true, &whitePlastic, getTransformMatrix(vec3(0, 3, 0), vec3(1, 1, 1), 0, vec3(0.7, 0.2, 0.7)));
  cube(true, &whitePlastic, getTransformMatrix(vec3(0, -3, 0), vec3(1, 1, 1), 0, vec3(0.7, 0.2, 0.7)));

  textures.at("spaceship").bindTexture();
  
  cube(true, &chrome, getTransformMatrix(vec3(0, 1.2, 0), vec3(1, 1, 1), 0, vec3(0.55, 0.2, 0.55)));
  cube(true, &chrome, getTransformMatrix(vec3(0, -1.2, 0), vec3(1, 1, 1), 0, vec3(0.55, 0.2, 0.55)));
  cylinder(true, &whitePlastic, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(0.5, 0.5, 0.5)));

  glPushMatrix();
  glMultMatrixf(value_ptr(glm::rotate(mat4(1.0f), glm::radians(90.0f), vec3(0, 0, 1))));
  tetrahedron(true, &whitePlastic, getTransformMatrix(vec3(5.5, 0.7, 0), vec3(1, 1, 1), 0, vec3(0.25, 1, 0.25)));
  cube(true, &chrome, getTransformMatrix(vec3(0, 0.7, 0), vec3(1, 1, 1), 0, vec3(0.3, 1, 0.3)));
  cylinder(true, &whitePlastic, getTransformMatrix(vec3(-1, -0.5, 0), vec3(1, 1, 1), 0, vec3(0.25, 1, 0.25)));
  glPopMatrix();

  cylinder(true, &whitePlastic, getTransformMatrix(vec3(-3, 0.5, 0), vec3(1, 1, 1), 0, vec3(0.25, 1, 0.25)));

  textures.at("solar_panel").bindTexture();
  cube(true, &whitePlastic, getTransformMatrix(vec3(2.7, -0.3, 0), vec3(1, 1, 1), 0, vec3(0.2, 1.2, 0.1)));
  cube(true, &whitePlastic, getTransformMatrix(vec3(3, -0.1, 0), vec3(1, 1, 1), 0, vec3(0.3, 1.2, 0.1)));
  cube(true, &whitePlastic, getTransformMatrix(vec3(6.4, 0.3, 0), vec3(1, 1, 1), 0, vec3(0.2, 1.2, 0.1)));

  glPopMatrix();
}

// Draws spaceship orbiting Markus space station
void Widget::drawSpaceship() {
  glPushMatrix();

  mat4 rotate = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time * -2)), vec3(0, 1, 0));
  mat4 axisTilt = glm::rotate(mat4(1.0f), static_cast<float>(glm::sin(_time / 250.0f) / 3.0f), vec3(1, 0, 1));
  mat4 translate = glm::translate(mat4(1.0f), vec3(2.5, 0, 0));
  mat4 spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time)), vec3(1, 1, 0));
  mat4 scale = glm::scale(mat4(1.0f), vec3(0.5, 0.2, 0.5));

  glMultMatrixf(value_ptr(rotate * axisTilt * translate * spin * scale));

  cylinder(false, &blackPlastic, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(0.4, 0.7, 0.4)));
  cube(false, &ruby, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(0.5, 0.2, 0.5)));
  cube(false, &ruby, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(0.1, 2.5, 0.1)));

  glPopMatrix();
}

// Draws planet and orbiting moons
void Widget::drawPlanet() {
  glPushMatrix();

  textures.at("map").bindTexture();
  
  // Main orbit maths - map texture
  marcRotation = glm::rotate(marcRotation, glm::radians(static_cast<float>(marcSpeed / 2.50f)), vec3(0, 1, 0));
  mat4 translate = glm::translate(mat4(1.0f), vec3(7, 0, 0));
  mat4 spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / 2.0f)), vec3(1, 1, 0));

  glMultMatrixf(value_ptr(marcRotation * translate * spin));

  tetrahedron(true, &whitePlastic, mat4(1.0f));

  textures.at("marc_de_kamps").bindTexture();

  // Secondary orbit maths - marc de kamps moon
  mat4 rotate = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / 3)), vec3(0, 1, 0));
  translate = glm::translate(mat4(1.0f), vec3(2, 0, 0));
  spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / 2)), vec3(1, 1, 0));
  mat4 scale = glm::scale(mat4(1.0f), vec3(0.4, 0.4, 0.4));

  glPushMatrix();
  glMultMatrixf(value_ptr(rotate * translate * spin * scale));

  cube(true, &whitePlastic, mat4(1.0f));

  textures.at("asteroid").bindTexture();

  // Third orbit maths - tetrahedron asteroid
  rotate = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / -2.0f)), vec3(0, 1, 0));
  translate = glm::translate(mat4(1.0f), vec3(2, 0, 0));
  spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / -1.0f)), vec3(0, 1, 1));
  scale = glm::scale(mat4(1.0f), vec3(0.6, 0.5, 0.45));

  glPushMatrix();
  glMultMatrixf(value_ptr(rotate * translate * spin * scale));

  tetrahedron(true, &whitePlastic, mat4(1.0f));

  // Fourth orbit maths - green cylinder planet
  rotate = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time * 3.0f)), vec3(0, 1, 0));
  translate = glm::translate(mat4(1.0f), vec3(1.5, 0, 0));
  spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time * -1.0f)), vec3(0, 1, 1));
  scale = glm::scale(mat4(1.0f), vec3(0.45, 0.2, 0.45));

  cylinder(false, &greenRubber, rotate * translate * spin * scale);

  glPopMatrix();
  glPopMatrix();
  glPopMatrix();
}

// Draws alien thing
void Widget::drawAlien() {
  glPushMatrix();

  mat4 rotate = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / 2.50f)), vec3(0, 0, 1));
  mat4 translate = glm::translate(mat4(1.0f), vec3(0, 6, 0));
  mat4 spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>(_time / 4.0f)), vec3(1, 1, 0));
  mat4 scale = glm::scale(mat4(1.0f), vec3(0.45, 0.8, 0.45));

  glMultMatrixf(value_ptr(rotate * translate * spin * scale));

  textures.at("alien").bindTexture();
  cylinder(true, &whitePlastic, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(1, 0.7, 1)));

  textures.at("eye").bindTexture();
  cylinder(true, &whitePlastic, getTransformMatrix(vec3(0, 0, 0), vec3(1, 1, 1), 0, vec3(0.9, 0.8, 0.9)));
  cylinder(true, &whitePlastic, getTransformMatrix(vec3(0, 0, 0), vec3(1, 0, 0), 90.0f, vec3(0.8, 0.4, 0.6)));
  
  glPopMatrix();
}

// Draws whole asteroid belt
void Widget::drawAsteroids() {
  glPushMatrix();
  textures.at("asteroid").bindTexture();

  // Overall Asteroid belt orbit rotation and spin
  asteroidRotation = glm::rotate(asteroidRotation, glm::radians(static_cast<float>(asteroidSpeed / 8.0f)), vec3(0, 1, 0));
  mat4 spin = glm::rotate(mat4(1.0f), static_cast<float>(glm::sin(_time / 350.0f) / 6.0f), vec3(1, 0, 1));

  glMultMatrixf(value_ptr(asteroidRotation * spin));

  // Draw individual asteroids
  for (Asteroid asteroid : asteroids) {
    glPushMatrix();

    mat4 translate = glm::translate(mat4(1.0f), vec3(asteroid.x, asteroid.y, asteroid.z));
    spin = glm::rotate(mat4(1.0f), glm::radians(static_cast<float>((_time / 7.0f) * asteroid.spinSpeed)), asteroid.spinAxis);
    glMultMatrixf(value_ptr(translate * spin * glm::scale(mat4(1.0f), asteroid.scale)));

    tetrahedron(true, &whitePlastic, mat4(1.0f));

    glPopMatrix();
  }
  glPopMatrix();
}

// Generates asteroids initially
void Widget::generateAsteroids() {

  int asteroidNumber = rand() % 70 + 100;
  float beltRadius = 11;
  vec3 previousLocation = vec3(beltRadius, 0, 0);

  // Generate individual asteroids here, between 50 and 100
  for (int i = 0; i < asteroidNumber; i++) {

    // Get rotated location around orbit circle
    vec3 nextLocation = glm::rotate(previousLocation, glm::radians(360.0f / asteroidNumber), vec3(0, 1, 0));
    float x = (rand() % 100 - 50) / 40.0f + nextLocation.x;
    float y = (rand() % 100 - 50) / 40.0f + nextLocation.y;
    float z = (rand() % 100 - 50) / 40.0f + nextLocation.z;

    // Generate random spin variations
    vec3 randomSpinAxis = vec3(rand() % 2, rand() % 2, rand() % 2);
    int randomSpinSpeed = rand() % 10 + 1;

    // Generate random scale
    vec3 randomScale = vec3((rand() % 60 + 16) / 100.0f, (rand() % 60 + 16) / 100.0f, (rand() % 60 + 16) / 100.0f);
    asteroids.push_back(Asteroid(x, y, z, randomSpinAxis, randomSpinSpeed, randomScale));

    previousLocation = nextLocation;
  }
}

// Sets and updates lighting position
void Widget::processLighting() {
  
  GLfloat lightPos[] = {lightPosition.x, lightPosition.y, lightPosition.z, lightPosition.w};

	glEnable(GL_LIGHTING); // enable lighting in general
  glEnable(GL_LIGHT0);   // each light source must also be enabled
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
}

// Draws the current position of the light in the scene in lines
void Widget::drawLightPosition() {
  glDisable(GL_LIGHTING);
  glPushMatrix();

  glBegin(GL_LINES);

  std::vector<vec3> vertices;
  vertices.push_back(vec3(lightPosition.x - 0.2, lightPosition.y, lightPosition.z));
  vertices.push_back(vec3(lightPosition.x + 0.2, lightPosition.y, lightPosition.z));
  vertices.push_back(vec3(lightPosition.x, lightPosition.y - 0.2, lightPosition.z));
  vertices.push_back(vec3(lightPosition.x, lightPosition.y + 0.2, lightPosition.z));
  vertices.push_back(vec3(lightPosition.x, lightPosition.y, lightPosition.z - 0.2));
  vertices.push_back(vec3(lightPosition.x, lightPosition.y, lightPosition.z + 0.2));

  // Draw white lines between all these vertices
  glColor4f(1, 1, 1, 0.75);
  for (vec3 vertex1 : vertices) {
    for (vec3 vertex2 : vertices) {
      if (vertex1 != vertex2) {
        glVertex3f(vertex1.x, vertex1.y, vertex1.z);
        glVertex3f(vertex2.x, vertex2.y, vertex2.z);
      }
    }
  }

  glEnd();
  glPopMatrix();
  glEnable(GL_LIGHTING);
}

// Draw x, y and z axis for debug purposes
// +ve x is red, -ve x is pink
// +ve y is green, -ve y is yellow
// +ve z is blue, -ve z is cyan
void Widget::drawAxis() {
  glDisable(GL_LIGHTING);

  // Positive axis lines
  int lineScalar = 1;
  drawLine({0, 0, 0}, {10*lineScalar, 0, 0}, {1, 0, 0, 0.75});
  drawLine({0, 0, 0}, {0, 10*lineScalar, 0}, {0, 1, 0, 0.75});
  drawLine({0, 0, 0}, {0, 0, 10*lineScalar}, {0, 0, 1, 0.75});

  // Negative axis lines
  drawLine({0, 0, 0}, {-10*lineScalar, 0, 0}, {1, 0, 0.75, 0.75});
  drawLine({0, 0, 0}, {0, -10*lineScalar, 0}, {1, 1, 0, 0.75});
  drawLine({0, 0, 0}, {0, 0, -10*lineScalar}, {0, 1, 1, 0.75});

  glEnable(GL_LIGHTING);
}

// Called every time the widget is resized
void Widget::resizeGL(int w, int h) {

	// Set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

  // Set perspective camera to use aspect ratio defined by width / height
  gluPerspective(45, (float) w / (float) h, 0.001, 100);
}

// Method run on a key press event
void Widget::keyPressEvent(QKeyEvent *event) {
  if (event->key() == Qt::Key_W) {
    cameraMoveable.MoveForward();
  }
  else if (event->key() == Qt::Key_A) {
    cameraMoveable.MoveLeft();
  }
  else if (event->key() == Qt::Key_S) {
    cameraMoveable.MoveBackward();
  }
  else if (event->key() == Qt::Key_D) {
    cameraMoveable.MoveRight();
  }
}

// Method run on a mouse press
void Widget::mousePressEvent(QMouseEvent *event) {

  if (event->button() == Qt::RightButton) {

    // Hide cursor if mouse move is enabled
    if (mouseMoveEnabled) {
      previousMouseX = -1;
      previousMouseY = -1;
      QCursor cursor(Qt::ArrowCursor);
      QApplication::setOverrideCursor(cursor);
      QApplication::changeOverrideCursor(cursor);
    }

    else {
      QCursor cursor(Qt::BlankCursor);
      QApplication::setOverrideCursor(cursor);
      QApplication::changeOverrideCursor(cursor);
      QCursor::setPos(this->mapToGlobal(this->rect().center()));
    }

    mouseMoveEnabled = !mouseMoveEnabled;
  }
}

// Processes mouse movement from widget's side, then calls camera to adjust
void Widget::processMouseMove() {

  if (mouseMoveEnabled) {
    // Get global mouse position and convert to widget coordinates
    QPoint p = QCursor::pos();
    p = this->mapFromGlobal(p);

    if (p.x() != this->rect().center().x() && p.y() != this->rect().center().y()) {
      camera.ProcessMouseMove(this->rect().center().x(), this->rect().center().y(), p.x(), p.y());
      QCursor::setPos(this->mapToGlobal(this->rect().center()));
    }
  }
}

// SLOT for asteroid speed slider
void Widget::asteroidSliderUpdate(int i) {
  asteroidSpeed = (float) i / 10;
}

// SLOT for marc speed slider
void Widget::marcSliderUpdate(int i) {
  marcSpeed = (float) i / 10;
}

// SLOT for light X pos slider
void Widget::sliderXUpdate(int i) {
  lightPosition.x = (float) i / 10;
}

// SLOT for light Y pos slider
void Widget::sliderYUpdate(int i) {
  lightPosition.y = (float) i / 10;
}

// SLOT for light Z pos slider
void Widget::sliderZUpdate(int i) {
  lightPosition.z = (float) i / 10;
}

// SLOT for toggle draw light position checkbox
void Widget::toggleLightPosition(int value) {
  if (value == 0) {
    showLightPosition = false;
  }
  else {
    showLightPosition = true;
  }
}

// SLOT for toggle axis checkbox
void Widget::toggleAxis(int value) {
  if (value == 0) {
    showAxis = false;
  }
  else {
    showAxis = true;
  }
}

// SLOT for toggling the wireframe
void Widget::toggleWireframe(int value) {
  if (value == 0) {
    wireframeEnabled = false;
  }
  else {
    wireframeEnabled = true;
  }
}

// SLOT for QTimer which updates local member var _time and repaints window
void Widget::updateTime() {
  _time += 1.0;
  this->repaint();
}

// Returns a transformation matrix which applies the given translation, rotation and scale
mat4 Widget::getTransformMatrix(vec3 translation, vec3 rotation, float angle, vec3 scale) {
  return glm::translate(glm::rotate(glm::scale(mat4(1.0f), scale), glm::radians(angle), rotation), translation);
}

// Draws a straight line from vertex1 to vertex2
void Widget::drawLine(vec3 vertex1, vec3 vertex2, std::vector<float> colour) {
  glBegin(GL_LINES);
  glColor4f(colour[0], colour[1], colour[2], colour[3]);
  glVertex3f(vertex1.x, vertex1.y, vertex1.z);
  glVertex3f(vertex2.x, vertex2.y, vertex2.z);
  glEnd();
}

// Draws a unit cube at 0,0
void Widget::cube(bool textured, const materialStruct* p_front, mat4 transformMatrix) {

  if (textured) {
    glEnable(GL_TEXTURE_2D);
  }

  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glMultMatrixf(value_ptr(transformMatrix));

  setGLMaterial(p_front);
  glBegin(GL_TRIANGLES);

  // Face 1 - facing +ve z
  glNormal3f(0, 0, 1);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[0].x, unitCube[0].y, unitCube[0].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[1].x, unitCube[1].y, unitCube[1].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[3].x, unitCube[3].y, unitCube[3].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[1].x, unitCube[1].y, unitCube[1].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[2].x, unitCube[2].y, unitCube[2].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[3].x, unitCube[3].y, unitCube[3].z);

  // Face 2 - facing -ve x
  glNormal3f(-1, 0, 0);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[0].x, unitCube[0].y, unitCube[0].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[3].x, unitCube[3].y, unitCube[3].z);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[4].x, unitCube[4].y, unitCube[4].z);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[4].x, unitCube[4].y, unitCube[4].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[3].x, unitCube[3].y, unitCube[3].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[7].x, unitCube[7].y, unitCube[7].z);

  // Face 3 - facing -ve z
  glNormal3f(0, 0, -1);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[4].x, unitCube[4].y, unitCube[4].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[7].x, unitCube[7].y, unitCube[7].z);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[7].x, unitCube[7].y, unitCube[7].z);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[6].x, unitCube[6].y, unitCube[6].z);

  // Face 4 - facing +ve x
  glNormal3f(1, 0, 0);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[1].x, unitCube[1].y, unitCube[1].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[2].x, unitCube[2].y, unitCube[2].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[6].x, unitCube[6].y, unitCube[6].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[2].x, unitCube[2].y, unitCube[2].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);

  // Face 5 - facing -ve y
  glNormal3f(0, -1, 0);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[0].x, unitCube[0].y, unitCube[0].z);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[4].x, unitCube[4].y, unitCube[4].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[0].x, unitCube[0].y, unitCube[0].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[1].x, unitCube[1].y, unitCube[1].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[5].x, unitCube[5].y, unitCube[5].z);

  // Face 6 - facing +ve y
  glNormal3f(0, 1, 0);
  glTexCoord2f(0, 0);
  glVertex3f(unitCube[3].x, unitCube[3].y, unitCube[3].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[2].x, unitCube[2].y, unitCube[2].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[7].x, unitCube[7].y, unitCube[7].z);
  glTexCoord2f(0, 1);
  glVertex3f(unitCube[7].x, unitCube[7].y, unitCube[7].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitCube[2].x, unitCube[2].y, unitCube[2].z);
  glTexCoord2f(1, 1);
  glVertex3f(unitCube[6].x, unitCube[6].y, unitCube[6].z);

  glEnd();

  if (textured) {
    glDisable(GL_TEXTURE_2D);
  }

  // Draw wireframe if enabled
  if (wireframeEnabled && p_front != &wireframe) {
    glDisable(GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    cube(false, &wireframe, glm::scale(mat4(1.0f), vec3(1.005, 1.005, 1.005)));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  glPopMatrix();
}

// Draws a unit equilateral triangle at 0,0
void Widget::triangle(bool textured, const materialStruct* p_front, mat4 transformMatrix) {

  if (textured) {
    glEnable(GL_TEXTURE_2D);
  }

  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glMultMatrixf(value_ptr(transformMatrix));

  setGLMaterial(p_front);
  glBegin(GL_TRIANGLES);

  glNormal3f(0, 0, 1);
  glTexCoord2f(0, 0);
  glVertex3f(unitTriangle[0].x, unitTriangle[0].y, unitTriangle[0].z);
  glTexCoord2f(0.5, 1);
  glVertex3f(unitTriangle[1].x, unitTriangle[1].y, unitTriangle[1].z);
  glTexCoord2f(1, 0);
  glVertex3f(unitTriangle[2].x, unitTriangle[2].y, unitTriangle[2].z);

  glEnd();

  if (textured) {
    glDisable(GL_TEXTURE_2D);
  }

  // Draw wireframe if enabled
  if (wireframeEnabled && p_front != &wireframe) {
    glDisable(GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    triangle(false, &wireframe, glm::scale(mat4(1.0f), vec3(1.005, 1.005, 1.005)));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  glPopMatrix();
}

// Draws a unit circle at 0,0
void Widget::circle(bool textured, const materialStruct* p_front, mat4 transformMatrix) {

  if (textured) {
    glEnable(GL_TEXTURE_2D);
  }

  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glMultMatrixf(value_ptr(transformMatrix));

  setGLMaterial(p_front);
  glBegin(GL_TRIANGLES);

  glNormal3f(0, 0, 1);
  float radius = 0.5;
  vec3 previousVertex = vec3(0, radius, 0);
  vec3 nextVertex;

  // Create circle out of triangles by fanning around the circle
  for (int i = 0; i < circleFidelity; i++) {

    // Calculate next vertex position
    nextVertex = glm::rotate(previousVertex, glm::radians(360.0f / circleFidelity), vec3(0, 0, 1));

    // Draw triangle
    glTexCoord2f(0.5, 0.5);
    glVertex3f(0, 0, 0);
    glTexCoord2f(previousVertex.x + 0.5, previousVertex.y + 0.5);
    glVertex3f(previousVertex.x, previousVertex.y, previousVertex.z);
    glTexCoord2f(nextVertex.x + 0.5, nextVertex.y + 0.5);
    glVertex3f(nextVertex.x, nextVertex.y, nextVertex.z);

    // Update previous vertex
    previousVertex = nextVertex;
  }

  glEnd();

  if (textured) {
    glDisable(GL_TEXTURE_2D);
  }

  // Draw wireframe if enabled
  if (wireframeEnabled && p_front != &wireframe) {
    glDisable(GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    circle(false, &wireframe, glm::scale(mat4(1.0f), vec3(1.005, 1.005, 1.005)));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  glPopMatrix();
}

// Draws a cyclinder at 0,0
void Widget::cylinder(bool textured, const materialStruct* p_front, glm::mat4 transformMatrix) {

  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glMultMatrixf(value_ptr(transformMatrix));

  // Draw ends of cyclinder, also making sure wireframe for circles are not drawn twice
  if (p_front == &wireframe ) {
    circle(textured, p_front, getTransformMatrix(vec3(0, 0, 1), vec3(1, 0, 0), 90.0f, vec3(1, 1, 1)));
    circle(textured, p_front, getTransformMatrix(vec3(0, 0, 1), vec3(1, 0, 0), -90.0f, vec3(1, 1, 1)));
  }
  else {
    bool wireframeOn = wireframeEnabled;
    wireframeEnabled = false;
    circle(textured, p_front, getTransformMatrix(vec3(0, 0, 1), vec3(1, 0, 0), 90.0f, vec3(1, 1, 1)));
    circle(textured, p_front, getTransformMatrix(vec3(0, 0, 1), vec3(1, 0, 0), -90.0f, vec3(1, 1, 1)));
    wireframeEnabled = wireframeOn;
  }
  
  if (textured) {
    glEnable(GL_TEXTURE_2D);
  }

  setGLMaterial(p_front);

  glBegin(GL_TRIANGLES);
  
  float radius = 0.5;
  vec3 previousVertex = vec3(0, -1, radius);
  vec3 previousNormal = vec3(0, 0, 1);
  vec3 nextVertex;
  vec3 nextNormal;
  float prevTexCoordX = 0;
  float nextTexCoordX;

  for (int i = 0; i < circleFidelity; i++) {

    // Calculate next vertex position
    nextVertex = glm::rotate(previousVertex, glm::radians(360.0f / circleFidelity), vec3(0, 1, 0));
    nextNormal = nextVertex - vec3(0, -1, 0);

    // Working out texture coordinates for points around the cyclinder
    nextTexCoordX = glm::degrees(glm::acos(glm::dot(glm::normalize(vec2(0, 1)), glm::normalize(vec2(nextNormal.x, nextNormal.z)))));
    if (nextVertex.x <= 0 && i != 0) {
      nextTexCoordX = 360 - nextTexCoordX;
    }
    if (i+1 == circleFidelity) {
      nextTexCoordX = 360;
    }
    nextTexCoordX = nextTexCoordX / 360;

    // Draw first triangle counter-clockwise
    glNormal3f(previousNormal.x, previousNormal.y, previousNormal.z);
    glTexCoord2f(prevTexCoordX, 0);
    glVertex3f(previousVertex.x, previousVertex.y, previousVertex.z);
    glNormal3f(nextNormal.x, nextNormal.y, nextNormal.z);
    glTexCoord2f(nextTexCoordX, 1);
    glVertex3f(nextVertex.x, nextVertex.y + 2, nextVertex.z);
    glNormal3f(previousNormal.x, previousNormal.y, previousNormal.z);
    glTexCoord2f(prevTexCoordX, 1);
    glVertex3f(previousVertex.x, previousVertex.y + 2, previousVertex.z);
    
    // Draw second triangle counter-clockwise
    glNormal3f(previousNormal.x, previousNormal.y, previousNormal.z);
    glTexCoord2f(prevTexCoordX, 0);
    glVertex3f(previousVertex.x, previousVertex.y, previousVertex.z);
    glNormal3f(nextNormal.x, nextNormal.y, nextNormal.z);
    glTexCoord2f(nextTexCoordX, 0);
    glVertex3f(nextVertex.x, nextVertex.y, nextVertex.z);
    glTexCoord2f(nextTexCoordX, 1);
    glVertex3f(nextVertex.x, nextVertex.y + 2, nextVertex.z);
    
    // Update previous normal and vertex
    previousNormal = nextNormal;
    previousVertex = nextVertex;
    prevTexCoordX = nextTexCoordX;
  }

  glEnd();

  if (textured) {
    glDisable(GL_TEXTURE_2D);
  }

  // Draw wireframe if enabled
  if (wireframeEnabled && p_front != &wireframe) {
    glDisable(GL_TEXTURE_2D);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    cylinder(false, &wireframe, glm::scale(mat4(1.0f), vec3(1.005, 1.005, 1.005)));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  glPopMatrix();
}

// Draws a tetrahedron between -0.5,-0.5,0.5 and 0.5,0.5,-0.5
void Widget::tetrahedron(bool textured, const materialStruct* p_front, mat4 transformMatrix) {

  if (textured) {
    glEnable(GL_TEXTURE_2D);
  }

  glMatrixMode(GL_MODELVIEW);

  glPushMatrix();
  glMultMatrixf(value_ptr(transformMatrix));

  setGLMaterial(p_front);
  glBegin(GL_TRIANGLES);

  vec3 vertex1 = vec3(-0.5, -0.5, 0.5);
  vec3 vertex2 = vec3(-0.5, 0.5, -0.5);
  vec3 vertex3 = vec3(0.5, -0.5, -0.5);
  vec3 vertex4 = vec3(0.5, 0.5, 0.5);

  // Calculate normals for each face
  vec3 normal1 = glm::cross(vertex3 - vertex2, vertex3 - vertex4);
  vec3 normal2 = -1.0f * glm::cross(vertex3 - vertex1, vertex3 - vertex4);
  vec3 normal3 = glm::cross(vertex2 - vertex1, vertex2 - vertex4);
  vec3 normal4 = -1.0f * glm::cross(vertex2 - vertex1, vertex2 - vertex3);

  // Face 1 - opposite vertex 1
  glNormal3f(normal1.x, normal1.y, normal1.z);
  glTexCoord2f(0, 0);
  glVertex3f(vertex2.x, vertex2.y, vertex2.z);
  glTexCoord2f(0.5, 1);
  glVertex3f(vertex3.x, vertex3.y, vertex3.z);
  glTexCoord2f(1, 0);
  glVertex3f(vertex4.x, vertex4.y, vertex4.z);

  // Face 2 - opposite vertex 2
  glNormal3f(normal2.x, normal2.y, normal2.z);
  glTexCoord2f(0, 0);
  glVertex3f(vertex1.x, vertex1.y, vertex1.z);
  glTexCoord2f(1, 0);
  glVertex3f(vertex3.x, vertex3.y, vertex3.z);
  glTexCoord2f(0.5, 1);
  glVertex3f(vertex4.x, vertex4.y, vertex4.z);

  // Face 3 - opposite vertex 3
  glNormal3f(normal3.x, normal3.y, normal3.z);
  glTexCoord2f(0.5, 1);
  glVertex3f(vertex1.x, vertex1.y, vertex1.z);
  glTexCoord2f(0, 0);
  glVertex3f(vertex2.x, vertex2.y, vertex2.z);
  glTexCoord2f(1, 0);
  glVertex3f(vertex4.x, vertex4.y, vertex4.z);

  // Face 4 - opposite vertex 4
  glNormal3f(normal4.x, normal4.y, normal4.z);
  glTexCoord2f(1, 0);
  glVertex3f(vertex1.x, vertex1.y, vertex1.z);
  glTexCoord2f(0.5, 1);
  glVertex3f(vertex2.x, vertex2.y, vertex2.z);
  glTexCoord2f(0, 0);
  glVertex3f(vertex3.x, vertex3.y, vertex3.z);

  glEnd();

  if (textured) {
    glDisable(GL_TEXTURE_2D);
  }

  // Draw wireframe if enabled
  if (wireframeEnabled && p_front != &wireframe) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    tetrahedron(false, &wireframe, glm::scale(mat4(1.0f), vec3(1.005, 1.005, 1.005)));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  glPopMatrix();
} 

// Sets the current material given a materialStruct
void Widget::setGLMaterial(const materialStruct* p_front) {
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
}