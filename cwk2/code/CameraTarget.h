#pragma once

#include "../libs/glm/glm/glm.hpp"
#include <vector>
#include "Camera.h"

// Derived class from Camera, for camera with movement with target location to fix look on
class CameraTarget : public Camera {
    public:
        CameraTarget(std::vector<float> position, std::vector<float> target);
        CameraTarget() = default;
        glm::mat4 GetLookAtMatrix();

        glm::vec3 cameraTarget;
        glm::vec3 cameraDirection;
};