#pragma once

#include <string>

class Texture {
	public:
        // Methods
		Texture(std::string fileName);
        void bindTexture();

        // Member variables
		std::string fileName;
        std::string path = std::string("../assets/textures/");
        unsigned int textureID;
};