#include "Window.h"
#include <iostream>
#include <QColor>

Window::Window(QWidget *parent): QWidget(parent) {
	
	// Create the window layout and controls layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	controlsLayout = new QFormLayout();

	// Create main widget
	sceneWidget = new Widget(this);
	windowLayout->addWidget(sceneWidget, 5, 0);
	windowLayout->addLayout(controlsLayout);

	// Create sliders
	asteroidSpeedSlider = new QSlider(Qt::Horizontal);
	asteroidSpeedSlider->setMinimum(-100);
	asteroidSpeedSlider->setValue(30);
	asteroidSpeedSlider->setMaximum(100);

	marcSpeedSlider = new QSlider(Qt::Horizontal);
	marcSpeedSlider->setMinimum(-100);
	marcSpeedSlider->setValue(40);
	marcSpeedSlider->setMaximum(100);

	lightSliderX = new QSlider(Qt::Horizontal);
	lightSliderX->setMinimum(-100);
	lightSliderX->setValue(10);
	lightSliderX->setMaximum(100);

	lightSliderY = new QSlider(Qt::Horizontal);
	lightSliderY->setMinimum(-100);
	lightSliderY->setValue(30);
	lightSliderY->setMaximum(100);

	lightSliderZ = new QSlider(Qt::Horizontal);
	lightSliderZ->setMinimum(-100);
	lightSliderZ->setValue(30);
	lightSliderZ->setMaximum(100);

	// Create checkboxes
	lightPositionToggle = new QCheckBox(this);
	axisToggle = new QCheckBox(this);
	wireframeToggle = new QCheckBox(this);
	
	// Create colour dialog button
	colourButton = new QPushButton("Choose", this);
	colourPicker = new QColorDialog();

	pTimer = new QTimer;
    pTimer->start(10);

	// Connect signals and slots for Qt
    connect(pTimer, SIGNAL(timeout()), sceneWidget, SLOT(updateTime()));
	connect(asteroidSpeedSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(asteroidSliderUpdate(int)));
	connect(marcSpeedSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(marcSliderUpdate(int)));
	connect(lightSliderX, SIGNAL(valueChanged(int)), sceneWidget, SLOT(sliderXUpdate(int)));
	connect(lightSliderY, SIGNAL(valueChanged(int)), sceneWidget, SLOT(sliderYUpdate(int)));
	connect(lightSliderZ, SIGNAL(valueChanged(int)), sceneWidget, SLOT(sliderZUpdate(int)));
	connect(lightPositionToggle, SIGNAL(stateChanged(int)), sceneWidget, SLOT(toggleLightPosition(int)));
	connect(axisToggle, SIGNAL(stateChanged(int)), sceneWidget, SLOT(toggleAxis(int)));
	connect(wireframeToggle, SIGNAL(stateChanged(int)), sceneWidget, SLOT(toggleWireframe(int)));
	connect(colourButton, SIGNAL(clicked(bool)), this, SLOT(showColourDialog(bool)));

	// Add controls to the control layout
	controlsLayout->addRow("Asteroid orbit speed ", asteroidSpeedSlider);
	controlsLayout->addRow("Planet Marc orbit speed ", marcSpeedSlider);
	controlsLayout->addRow("Light position X ", lightSliderX);
	controlsLayout->addRow("Light position Y ", lightSliderY);
	controlsLayout->addRow("Light position Z ", lightSliderZ);
	controlsLayout->addRow("Show light position ", lightPositionToggle);
	controlsLayout->addRow("Show scene axis ", axisToggle);
	controlsLayout->addRow("Show wireframe ", wireframeToggle);
	controlsLayout->addRow("Select wireframe colour ", colourButton);
}

// Destructor for Window object
Window::~Window() {
	delete asteroidSpeedSlider;
	delete marcSpeedSlider;
	delete lightSliderX;
	delete lightSliderY;
	delete lightSliderZ;
	delete lightPositionToggle;
	delete axisToggle;
	delete wireframeToggle;
	delete colourPicker;
	delete colourButton;
	delete sceneWidget;
	delete controlsLayout;
	delete windowLayout;
	delete pTimer;
}

// Resets all the interface elements
void Window::ResetInterface() {

	asteroidSpeedSlider->setMinimum(-100);
	asteroidSpeedSlider->setValue(30);
	asteroidSpeedSlider->setMaximum(100);

	marcSpeedSlider->setMinimum(-100);
	marcSpeedSlider->setValue(40);
	marcSpeedSlider->setMaximum(100);

	lightSliderX->setMinimum(-100);
	lightSliderX->setMaximum(100);
	lightSliderX->setValue(0);

	lightSliderY->setMinimum(-100);
	lightSliderY->setMaximum(100);
	lightSliderY->setValue(0);

	lightSliderZ->setMinimum(-100);
	lightSliderZ->setMaximum(100);
	lightSliderZ->setValue(0);
	
	// Force refresh
	sceneWidget->update();
	update();
}

void Window::showColourDialog(bool clicked) {
	// Launches the colour picker dialog box
	QColor colour = colourPicker->getColor();

	// If colour was selected, update wireframe material
	if (colour.isValid()) {
		float rgb[3] = {static_cast<float>(colour.red()) / 255.0f, static_cast<float>(colour.green()) / 255.0f, static_cast<float>(colour.blue()) / 255.0f};
		for (int i = 0; i < 3; i++) {
			sceneWidget->wireframe.ambient[i] = rgb[i];
			sceneWidget->wireframe.diffuse[i] = rgb[i];
			sceneWidget->wireframe.specular[i] = rgb[i];	
		}
	}
}