#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include "../libs/stb_image.h"
#include <iostream>
#include <GL/gl.h>

// Sets the current texture to the image specified by fileName
Texture::Texture(std::string fileName) {

    this->fileName = fileName;

    int width, height, channels;

    // Loading the texture
    fileName = path + fileName;
    stbi_set_flip_vertically_on_load(true);
    void *imageData = stbi_load(fileName.c_str(), &width, &height, &channels, STBI_rgb);

    if (!imageData) {
        std::cout << "Failure loading texture '" << fileName << "' (" << stbi_failure_reason() << ")" << std::endl;
    }
    else {

        // Binding and setting the texture for openGL
        glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        stbi_image_free(imageData);
    }
}

// Binds this texture to open gl
void Texture::bindTexture() {
    glBindTexture(GL_TEXTURE_2D, textureID);
}