#pragma once

#include <QGLWidget>
#include <QKeyEvent>
#include <QMouseEvent>
#include "CameraTarget.h"
#include "CameraMoveable.h"
#include "Camera.h"
#include "../libs/glm/glm/glm.hpp"
#include <vector>
#include <string>
#include <unordered_map>
#include "Materials.h"
#include "Texture.h"
#include "Asteroid.h"

class Widget: public QGLWidget {
	Q_OBJECT

	public:
		Widget(QWidget *parent);

		materialStruct wireframe {
			{ 1, 1, 1, 1},
			{ 1, 1, 1, 1},
			{ 1, 1, 1, 1},
			100
		};

		public slots:
		// Called by the sliders in the main window
		void asteroidSliderUpdate(int);
		void marcSliderUpdate(int);
		void sliderXUpdate(int);
		void sliderYUpdate(int);
		void sliderZUpdate(int);
		void toggleLightPosition(int);
		void toggleAxis(int);
		void toggleWireframe(int);
		void updateTime();
        
	protected:
		// called when OpenGL context is set up
		void initializeGL();
		// called every time the widget is resized
		void resizeGL(int w, int h);
		// called every time the widget needs painting
		void paintGL();
		// Called on Qt Key Press Event
		void keyPressEvent(QKeyEvent *event) override;
		void mousePressEvent(QMouseEvent *event) override;

	private:
		// Methods
		void triangle(bool textured, const materialStruct*, glm::mat4 transformMatrix = glm::mat4(1.0f));
		void cube(bool textured, const materialStruct*, glm::mat4 transformMatrix = glm::mat4(1.0f));
		void tetrahedron(bool textured, const materialStruct* p_front, glm::mat4 transformMatrix = glm::mat4(1.0f));
		void circle(bool textured, const materialStruct* p_front, glm::mat4 transformMatrix = glm::mat4(1.0f));
		void cylinder(bool textured, const materialStruct* p_front, glm::mat4 transformMatrix = glm::mat4(1.0f));
		void drawLine(glm::vec3 vertex1, glm::vec3 vertex2, std::vector<float> colour);
		void drawAxis();
		void drawLightPosition();
		void drawSkybox();
		void drawSpaceStation();
		void drawSpaceship();
		void drawPlanet();
		void drawAlien();
		void drawScene();
		void generateAsteroids();
		void drawAsteroids();
		glm::mat4 getTransformMatrix(glm::vec3 translation = glm::vec3(0, 0, 0), glm::vec3 rotation = glm::vec3(0, 0, 0), float angle = 0, glm::vec3 scale = glm::vec3(1));
		void processMouseMove();
		void setGLMaterial(const materialStruct*);
		void processLighting();
		void setTexture(std::string fileName);
		
		// Member variables
		double _angle = 0;
		double _time = 0; 
		CameraTarget cameraTarget;
		CameraMoveable cameraMoveable;
		Camera& camera;
		bool mouseMoveEnabled = false;
		int previousMouseX = -1;
		int previousMouseY = -1;
		glm::vec4 lightPosition = glm::vec4(1, 3, 3, 0);
		std::unordered_map<std::string, Texture> textures;
		std::vector<Asteroid> asteroids;
		bool showLightPosition = false;
		bool showAxis = false;
		bool wireframeEnabled = false;
		float asteroidSpeed = 3;
		float marcSpeed = 4;
		glm::mat4 asteroidRotation = glm::mat4(1.0f);
		glm::mat4 marcRotation = glm::mat4(1.0f);
};
