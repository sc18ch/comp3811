#pragma once

// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;

// Defining different materials below here

static materialStruct greenRubber = {
  { 0.4f , 0.45f, 0.4f , 1.0f },
  { 0.4f , 0.5f , 0.4f , 1.0f },
  { 0.04f, 0.7f , 0.04f, 1.0f },
  10.0f
};

static materialStruct ruby = {
  { 0.3745f  , 0.31175f , 0.31175f , 0.55f },
  { 0.61424f , 0.04136f , 0.04136f , 0.55f },
  { 0.727811f, 0.626959f, 0.626959f, 0.55f },
  76.8f
};

static materialStruct chrome = {
  { 0.4f, 0.4f, 0.4f, 1.0f },
  { 0.4f, 0.4f, 0.4f, 1.0f },
  { 0.774597f, 0.774597f, 0.774597f, 1.0f },
  80.8f
};

static materialStruct texture = {
  { 5.0f, 5.0f, 5.0f, 5.0f },
  { 0.0f, 0.0f, 0.0f, 0.0f },
  { 0.0f, 0.0f, 0.0f, 0.0f },
  0.0f
};

static materialStruct blackPlastic = {
  { 0.0f, 0.0f, 0.0f, 1.0f },
  { 0.01f, 0.01f, 0.01f, 1.0f },
  { 0.50f, 0.50f, 0.50f, 1.0f },
  32.0f
};

static materialStruct whitePlastic = { 
  { 0.9f, 0.9f, 0.9f, 1.0f },
  { 0.55f, 0.55f, 0.55f, 1.0f },
  { 0.70f, 0.70f, 0.70f, 1.0f },
  62.0f
};