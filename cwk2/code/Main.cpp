#include <QApplication>
#include <QVBoxLayout>
#include "Window.h"

int main(int argc, char *argv[]) {

	// Create the Qt application
	QApplication app(argc, argv);

	// Create master window
    Window *window = new Window(NULL);

	window->resize(1000, 900);
	window->show();
	app.exec();

	delete window;
	return 0; 
}
