#pragma once

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QLabel>
#include <QTimer>
#include <QBoxLayout>
#include <QFormLayout>
#include <QCheckBox>
#include <QColorDialog>
#include <QPushButton>
#include "Widget.h"

class Window: public QWidget { 
	Q_OBJECT
	
	public:
		Window(QWidget *parent);
		virtual ~Window();

		QColorDialog *colourPicker;

		public slots:
		void showColourDialog(bool);

	private:
		// Resets all the interface elements
		void ResetInterface();

		// Window layout
		QBoxLayout *windowLayout;
		QFormLayout *controlsLayout;

		// Main widget
		Widget *sceneWidget;

		// Sliders for orbit speed manipulation
		QSlider *asteroidSpeedSlider;
		QSlider *marcSpeedSlider;

		// Sliders for light position
		QSlider *lightSliderX;
		QSlider *lightSliderY;
		QSlider *lightSliderZ;

		// Checkboxes for toggling light pos and axis
		QCheckBox *lightPositionToggle;
		QCheckBox *axisToggle;
		QCheckBox *wireframeToggle;

		// Colour button for RGB colour picking
		QPushButton *colourButton;
		
		// Program timer
		QTimer *pTimer;
}; 
