#include "../libs/glm/glm/glm.hpp"
#include "../libs/glm/glm/ext/matrix_transform.hpp"
#include "CameraMoveable.h"
#include <vector>

using namespace std;

CameraMoveable::CameraMoveable(vector<float> position, float cameraSpeed) {

    // Set camera speed
    this->cameraSpeed = cameraSpeed;

    // Setting up camera position and other vectors
    cameraPosition = glm::vec3(position[0], position[1], position[2]);
    cameraFront = glm::vec3(0, 0, -1);
    cameraUp = glm::vec3(0, 1, 0);
}

// Return camera look at matrix
glm::mat4 CameraMoveable::GetLookAtMatrix() {
    return glm::lookAt(cameraPosition, cameraPosition + cameraFront, cameraUp);
}

// Processes mouse movements
void CameraMoveable::ProcessMouseMove(int previousX, int previousY, int x, int y) {

    float mouseSensitivity = 0.1;

    float mouseOffsetX = x - previousX;
    float mouseOffsetY = previousY - y;

    mouseOffsetX *= mouseSensitivity;
    mouseOffsetY *= mouseSensitivity;

    yaw += mouseOffsetX;
    pitch += mouseOffsetY;

    if (pitch > 89.0f) {
        pitch =  89.0f;
    }
    else if (pitch < -89.0f) {
        pitch = -89.0f;
    }

    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(direction);

}

// Moves camera forward along direction it faces
void CameraMoveable::MoveForward() {
    cameraPosition += cameraSpeed * cameraFront;
}

// Moves camera backward along direction it faces
void CameraMoveable::MoveBackward() {
    cameraPosition -= cameraSpeed * cameraFront;
}

// Strafes camera left
void CameraMoveable::MoveLeft() {
    cameraPosition -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}

// Strafes camera right
void CameraMoveable::MoveRight() {
    cameraPosition += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}