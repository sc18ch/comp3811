#ifndef _2DMATRIX_
#define _2DMATRIX_

#include <iostream>

class Matrix2D {
public:

  float a;
  float b;
  float c;
  float d;

  // Create Identity matrix if no values given
  Matrix2D(float a, float b, float c, float d):a(a), b(b), c(c), d(d){}
  Matrix2D():a(1), b(0), c(1), d(0){}

  float Determinant() {
    return a*d - b*c;
  }

  bool InverseExists() {
    return Determinant() != 0 ? true : false;
  }

  void PrintMatrix() {
    std::cout << "(" << a << ", " << b << "\n " << c << ", " << d << ")" << std::endl;
  }

  // Returns inverse matrix
  Matrix2D GetInverse() {
    if (!InverseExists()) {
      throw "No inverse exists";
    }
    return Matrix2D(d / Determinant(), (b / Determinant())* -1, (c / Determinant()) * -1, a / Determinant());
  }

  // Returns resulting matrix from matrix1 * matrix2
  static Matrix2D MultiplyByMatrix(Matrix2D matrix1, Matrix2D matrix2) {
    return Matrix2D(matrix1.a * matrix2.a + matrix1.b * matrix2.c,
      matrix1.a * matrix2.b + matrix1.b * matrix2.d,
      matrix1.c * matrix2.a + matrix1.d * matrix2.c,
      matrix1.c * matrix2.b + matrix1.d * matrix2.d);
  }

};

#endif
