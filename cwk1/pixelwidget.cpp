#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <fstream>
#include <math.h>
#include "pixelwidget.hpp"
#include <fstream>

void PixelWidget::DefinePixelValues() { //add pixels here; write methods like this for the assignments
  //SetPixel(3,50,RGBVal(255,0,0));
  //DrawLine(3.0f, 3.0f, 50.0f, 50.0f, RGBVal(255,255,255), RGBVal(255,255,255));
  //DrawLine(1.0f, 1.0f, 1.0f, 64.0f, RGBVal(0,255,0), RGBVal(0,0,255));
  //DrawLine(3.0f, 50.0f, 50.0f, 3.0f, RGBVal(255,255,255), RGBVal(255,255,255));
  //SetPixel(3,50,RGBVal(255,0,0));
  //DrawLine(30.2f, 50.9f, 5.2f, 3.7f, RGBVal(255,255,0), RGBVal(0,0,255));
  //DrawLine(0.0f, 0.0f, 2.0f, 2.0f, RGBVal(255,255,255), RGBVal(255,255,0));
  //DrawLine(69.0f, 0.0f, 0.0f, 69.0f);
  //DrawLine(0.0f, 0.0f, 69.0f, 69.0f, RGBVal(255,255,255), RGBVal(255,255,255));
  //DrawLine(4.0f, 50.0f, 50.0f, 3.0f, RGBVal(255,0,0), RGBVal(0,255,255));
  //DrawLine(14.0f, 1.0f, 50.0f, 60.0f, RGBVal(255,255,255), RGBVal(255,255,255));
  //DrawLine(1.0f, 69.0f, 69.0f, 1.0f);
  //DrawLine(10.0f, 10.0f, 23.0f, 69.0f, RGBVal(255,255,255), RGBVal(255,255,255));
  //DrawTriangle(1, 1, 0, 40, 40, 20, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  DrawTriangle(20, 3, 10, 42, 50, 64, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  //DrawTriangle(20.5f, 20.5f, 10.5f, 30.5f, 30.5f, 30.0f, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  //DrawTriangle(0, 0, 2, 0, 2, 68, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  //CreateComparisonFile(20, 3, 10, 42, 50, 64);
  //WriteToPPM();
}

// Draws a line between (x1,y1) and (x2,y2), interpolating from colour1 to colour2
void PixelWidget::DrawLine(float x1, float y1, float x2, float y2, RGBVal colour1, RGBVal colour2) {

  int minX;
  int maxX;
  int minY;
  int maxY;

  // Finding max and min xy integers between the two points
  if (x1 < x2) {
    minX = (int) ceilf(x1);
    maxX = (int) floorf(x2);
  }
  else {
    maxX = (int) floorf(x1);
    minX = (int) ceilf(x2);
  }

  if (y1 < y2) {
    minY = (int) ceilf(y1);
    maxY = (int) floorf(y2);
  }
  else {
    maxY = (int) floorf(y1);
    minY = (int) ceilf(y2);
  }
  
  // Iterate between max and min (x, y) ranges
  std::vector<double> tValuesX;
  std::vector<double> tValuesY;
  std::vector<double> tValuesAll;

  // Get t at x intersections when x2 != x1
  if (x2 != x1) {
    for (int x = minX; x <= maxX; x++) {
      tValuesX.push_back((x - x1) / (x2 - x1));
    }
  }
  // Get t at y intersections when y2 != y1
  if (y2 != y1) {
    for (int y = minY; y <= maxY; y++) {
      tValuesY.push_back((y - y1) / (y2 - y1));  
    }
  }

  // Merge arrays and sort by t ascending
  tValuesAll = tValuesX;
  tValuesAll.insert(tValuesAll.end(), tValuesY.begin(), tValuesY.end());
  std::sort(tValuesAll.begin(), tValuesAll.end());
  tValuesAll.insert(tValuesAll.begin(), 0);
  tValuesAll.insert(tValuesAll.end(), 1);


  // Find midpoints between intersections with integers coords and send those t values to SetPixelFromT
  for (int index = 0; index < tValuesAll.size()-1; index++) {
    float t = ((tValuesAll[index+1] - tValuesAll[index]) / 2) + tValuesAll[index];
    SetPixelFromT(x1, y1, x2, y2, colour1, colour2, t);
  }
}

// Sets a pixel on a given line, the point being based on the t value provided
void PixelWidget::SetPixelFromT(float x1, float y1, float x2, float y2, RGBVal colour1, RGBVal colour2, float t) {

  // Calculate this pixel's (x,y) on parameterised line
  float pixelX = (1-t) * x1 + t * x2;
  float pixelY = (1-t) * y1 + t * y2;

  // Making sure floating point precision does not hinder pixel setting
  float boundary = 0.00001;
  if (ceilf(pixelX + boundary) != ceilf(pixelX)) {
    pixelX = ceilf(pixelX);
  }
  else if (floorf(pixelX - boundary) != floorf(pixelX)) {
    pixelX = floorf(pixelX);
  }
  if (ceilf(pixelY + boundary) != ceilf(pixelY)) {
    pixelY = ceilf(pixelY);
  }
  else if (floorf(pixelY - boundary) != floorf(pixelY)) {
    pixelY = floorf(pixelY);
  }

  // Calculate this pixel's colour
  int redVal = (int) roundf((1-t) * colour1._red + t * colour2._red);
  int greenVal = (int) roundf((1-t) * colour1._green + t * colour2._green);
  int blueVal = (int) roundf((1-t) * colour1._blue + t * colour2._blue);
  
  SetPixel(pixelX, pixelY, RGBVal(redVal, greenVal, blueVal));
}

// Draws a triangle between (x1,y1), (x2,y2), (x3,y3) and interpolates 3 colours between the points
void PixelWidget::DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, RGBVal colour1, RGBVal colour2, RGBVal colour3) {

  float minX = fminf(x1, fminf(x2, x3));
  float maxX = fmaxf(x1, fmaxf(x2, x3));;
  float minY = fminf(y1, fminf(y2, y3));;
  float maxY = fmaxf(y1, fmaxf(y2, y3));;

  Matrix2D matrixT = Matrix2D(x1 - x3, x2 - x3, y1 - y3, y2 - y3);
  Matrix2D matrixTInverse = matrixT.GetInverse();
  
  // Checking all pixels in a bounded square around the triangle
  for (int x = (int) floorf(minX); x < (int) ceilf(maxX); x++) {
    for (int y = (int) floorf(minY); y < (int) ceilf(maxY); y++) {

      float alpha = matrixTInverse.a * (x - x3) + matrixTInverse.b * (y - y3);
      float beta = matrixTInverse.c * (x - x3) + matrixTInverse.d * (y - y3);
      float gamma = 1 - alpha - beta;

      // Checking if point is within the triangle
      if (alpha + beta <= 1 && alpha >= 0 && beta >= 0 && gamma >= 0) {
        int redVal = alpha * colour1._red + beta * colour2._red + gamma * colour3._red;
        int greenVal = alpha * colour1._green + beta * colour2._green + gamma * colour3._green;
        int blueVal = alpha * colour1._blue + beta * colour2._blue + gamma * colour3._blue;
        SetPixel(x, y, RGBVal(redVal, greenVal, blueVal));
      }
    }
  }

  // Drawing edges of the triangle
  DrawLine(x1, y1, x2, y2, colour1, colour2);
  DrawLine(x2, y2, x3, y3, colour2, colour3);
  DrawLine(x3, y3, x1, y1, colour3, colour1);
}

// Determines whether point is on the side of the line
bool PixelWidget::OnSideOfLine(float x1, float y1, float x2, float y2, float pointX, float pointY) {
  float normalX = -1 * (y2 - y1);
  float normalY = x2 - x1;

  float vectorX = pointX - x1;
  float vectorY = pointY - y1;

  // Calculate the dot product and return scalar
  return (vectorX * normalX + vectorY * normalY) <= 0;
}

// Checks whether a point (pointX, pointY) is inside the given triangle, using the half plane technique
bool PixelWidget::IsInside(float x1, float y1, float x2, float y2, float x3, float y3, float pointX, float pointY) {
  return OnSideOfLine(x1, y1, x2, y2, pointX, pointY) && OnSideOfLine(x2, y2, x3, y3, pointX, pointY) && OnSideOfLine(x3, y3, x1, y1, pointX, pointY);
}

// Given a triangle, produces a file with a list of all pixel's barycentric coordiantes and whether they are inside the triangle 
void PixelWidget::CreateComparisonFile(float x1, float y1, float x2, float y2, float x3, float y3) {
  
  Matrix2D matrixT = Matrix2D(x1 - x3, x2 - x3, y1 - y3, y2 - y3);
  Matrix2D matrixTInverse = matrixT.GetInverse();
  std::ofstream file;
  file.open("comparison_output.txt");

  // Calculating barycentric coordinate of each pixel and whether it is inside the triangle
  for (int x = 0; x < _n_horizontal; x++) {
    for (int y = 0; y < _n_vertical; y++) {
      float alpha = matrixTInverse.a * (x - x3) + matrixTInverse.b * (y - y3);
      float beta = matrixTInverse.c * (x - x3) + matrixTInverse.d * (y - y3);
      float gamma = 1 - alpha - beta;

      if (IsInside(x1, y1, x2, y2, x3, y3, x, y)) {
        file << "Pixel (x,y) = (" << x << ", " << y << ")" << " | alpha = " << alpha << " | beta = " << beta << " | gamma = " << gamma << " | Inside triangle = TRUE\n";
      }
      else {
        file << "Pixel (x,y) = (" << x << ", " << y << ")" << " | alpha = " << alpha << " | beta = " << beta << " | gamma = " << gamma << " | Inside triangle = FALSE\n";
      }    
    }
  }

  file.close();
}

// Writes the current image from vec rects to a PPM file
void PixelWidget::WriteToPPM() {

  std::ofstream file;
  file.open("image.ppm");
  file << "P3 " << _n_horizontal << " " << _n_vertical << " 255\n";

  int count = 0;

  for (int y = 0; y < _n_horizontal; y++) {
    for (int x = 0; x < _n_vertical; x++) {
      // Check to make sure line is within 70 character limit
      if (count % 4 == 0) {
        file << "\n";
      }
      file << _vec_rects[x][y]._red << " " << _vec_rects[x][y]._green << " " << _vec_rects[x][y]._blue << "    ";
      count++;
    }
  }

  file.close();
}

// -----------------Most code below can remain untouched -------------------------
// ------but look at where DefinePixelValues is called in paintEvent--------------

PixelWidget::PixelWidget(unsigned int n_vertical, unsigned int n_horizontal):
  _n_vertical   (n_vertical),
  _n_horizontal (n_horizontal),
  _vec_rects(0)
{
  // all pixels are initialized to black
     for (unsigned int i_col = 0; i_col < n_vertical; i_col++)
       _vec_rects.push_back(std::vector<RGBVal>(n_horizontal));
}

void PixelWidget::SetPixel(unsigned int i_column, unsigned int i_row, const RGBVal& rgb)
{
  
  // if the pixel exists, set it
  if (i_column < _n_vertical && i_row < _n_horizontal)
    _vec_rects[i_column][i_row] = rgb;
}

void PixelWidget::paintEvent( QPaintEvent * )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, false );

  // set window/viewport so that the size fits the screen, within reason
  p.setWindow(QRect(-1.,-1.,_n_vertical+2,_n_horizontal+2));
  int side = qMin(width(), height());  
  p.setViewport(0, 0, side, side);

  // black thin boundary around the cells
  QPen pen( Qt::black );
  pen.setWidth(0.);

  // here the pixel values defined by the user are set in the pixel array
  DefinePixelValues();

  for (unsigned int i_column = 0 ; i_column < _n_vertical; i_column++)
    for(unsigned int i_row = 0; i_row < _n_horizontal; i_row++){
      QRect rect(i_column,i_row,1,1);
      QColor c = QColor(_vec_rects[i_column][i_row]._red, _vec_rects[i_column][i_row]._green, _vec_rects[i_column][i_row]._blue);
    
      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
      p.setPen(pen);
      p.drawRect(rect);
    }

  // Pen for accuracy testing
  p.setPen(QPen(Qt::red, 0.2));
  
  //p.drawLine(20, 3, 50, 64);
  //p.drawLine(50, 64, 10, 42);
  //p.drawLine(10, 42, 20, 3);
  //p.drawLine(3, 50, 50, 3);
}