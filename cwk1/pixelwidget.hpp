#ifndef _PIXEL_WIDGET_
#define _PIXEL_WIDGET_

#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <vector>
#include "RGBVal.hpp"
#include "Matrix2D.hpp"


class PixelWidget : public QWidget {
public:

  void DrawLine(float x1, float y1, float x2, float y2, RGBVal colour1, RGBVal colour2);
  void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, RGBVal colour1, RGBVal colour2, RGBVal colour3);
  void SetPixelFromT(float x1, float y1, float x2, float y2, RGBVal colour1, RGBVal colour2, float t);
  bool IsInside(float x1, float y1, float x2, float y2, float x3, float y3, float pointX, float pointY);
  bool OnSideOfLine(float x1, float y1, float x2, float y2, float pointX, float pointY);
  void CreateComparisonFile(float x1, float y1, float x2, float y2, float x3, float y3);
  void WriteToPPM();
  
  // set the number of pixels that the widget is meant to display
  PixelWidget
  (
   unsigned int n_horizontal, // the first integer determines the number of horizontal pixels 
   unsigned int n_vertical    // the second integer determines the number of vertical pixels
   );

  // sets a pixel at the specified RGBVal value; ignores non-existing pixels without warning
  void SetPixel
  ( 
   unsigned int  i_x, // horizontal pixel coordinate
   unsigned int  i_y, // vertical pixel coordinate
   const RGBVal& c    // RBGVal object for RGB values
    );

  // Use the body of this function to experiment with rendering algorithms
  void DefinePixelValues();

protected:

  virtual void paintEvent(QPaintEvent*);


private:

  unsigned int _n_vertical;
  unsigned int _n_horizontal;

  std::vector<std::vector<RGBVal> > _vec_rects;
};

#endif

