file = open("comparison_output.txt", 'r')


array2d = []

for line in file:
    values = []
    for element in line.split("|")[1:]:
        values.append(element.split("=")[1].strip())
    
    array2d.append(values)

correct = []
incorrect = []

for point in array2d:
    inside = False
    a = float(point[0])
    b = float(point[1])
    g = float(point[2])
    if point[3] == "TRUE":
        inside = True

    valuesCheck = a + b <= 1 and a >= 0 and b >= 0 and g >= 0

    if valuesCheck == inside:
        correct.append(point)
    else:
        incorrect.append(point)


print("Number correct = " + str(len(correct)))
print("Number incorrect = " + str(len(incorrect)))